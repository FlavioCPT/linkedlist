/* ************************************************************************
 * Name: Flavio C. Palacios Tinoco
 * Assignment: 03
 * Purpose: Linked list definition
 * Notes:
 * *************************************************************************/
#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include "main.h"

//Nodes will be instances
//inside the "List" class.
struct Node{
    int id;
    Node *forward;
    Node *backward;
};

class List{
private:
    //Declared node counter.
    static int count;

    //Pointers able to point to a Node are defined.
    struct Node *createdNodePtr;
    struct Node *headptr;
    struct Node *tailptr;
    struct Node *tempptrOne;
    struct Node *tempptrTwo;

public:
    //CONSTRUCTOR.
    List();

    //MEMBER FUNCTIONS.
    //Adds node to list in ascending order
    //based on the fed "id" value.
    void addNode(int);

    //Capable of only deleting head, tail,
    //or middle node based on argument
    //representing desired Nth node to
    //delete (the 2nd,3rd,4th,5th,...).
    void deleteNode(int);

    //Prints list of nodes.
    void printList();

    //Gets "count".
    int getCount();
};
#endif
