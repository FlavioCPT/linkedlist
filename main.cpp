/* ************************************************************************
 * Name: Flavio C. Palacios Tinoco
 * Assignment: 03
 * Purpose: Main program
 * Notes: The only thing that bothered me from this assignment was being unable
 * to return my list object from a function defined in the "functions.cpp" file.
 * Otherwise, it works just fine. I ended up declaring the object in the "main.cpp".
 * From what I collected, it has to do something with my object going out of scope
 * as soon as the function called ends.
 * *************************************************************************/
#include"main.h"

//Static variable is initialized.
int List::count = 0;

int main(int argc, char **argv){

    //Fed command line arguments are checked for validity.
    bool appropriateArgs;
    appropriateArgs = checkArgs(argc, argv[1]);

    if(appropriateArgs == false){return 0;}
    else{
        //If all is in order, "argv[1]" is turned to an "int" and it will
        //stand for the size of an array of random integers.
        const int ARRAY_SIZE = atoi(argv[1]);
        int randomIntArray[ARRAY_SIZE];

        //Array is filled.
        randomIntGenerator(randomIntArray, ARRAY_SIZE);

        //Class is initialized and function call to add nodes is made.
        List nodeFactory;

        int idValue;
        for(int index = 0; index < ARRAY_SIZE; ++index){
            idValue = randomIntArray[index];
            nodeFactory.addNode(idValue);
        };

        //Actual testing of the linked list begins here.
        int countCopy;
        int remainder;
        cout << "Printing list for the first time." << endl;
        nodeFactory.printList();
        cout << endl << endl;

        cout << "Printing list after deleting head node." << endl;
        nodeFactory.deleteNode(1);
        nodeFactory.printList();
        cout << endl << endl;

        cout << "Printing list after deleting tail node." << endl;
        countCopy = nodeFactory.getCount();
        nodeFactory.deleteNode(countCopy);
        nodeFactory.printList();
        countCopy = nodeFactory.getCount();
        cout << endl << endl;

        cout << "Printing list after deleting middle node." << endl;
        remainder = countCopy % 2;
        if(remainder == 1){countCopy = (countCopy / 2) + 1;}
        else{countCopy = countCopy / 2;};
        nodeFactory.deleteNode(countCopy);
        nodeFactory.printList();
    };
    return 0;
}