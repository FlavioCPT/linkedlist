
/* ************************************************************************
 * Name: Flavio C. Palacios Tinoco
 * Assignment: 03
 * Purpose: Function definitions
 * Notes:
 * *************************************************************************/
#include "main.h"

//- - - - - - - - - - - - - - - - - -
//     ***INPUT VALIDATION***
//- - - - - - - - - - - - - - - - - -
bool checkArgs(int numOfArgs, char *value){
    //One bool is to check appropriate argument count and
    //the other is for ensuring proper "char" values.
    bool validArgsNumb = true;
    bool charsAreDigits = true;

    //No more than 2 arguments may be passed. The project exe name
    //and the digit in "char" form are enough.
    if (numOfArgs > 2){
        cout << "ERROR: Too many or no arguments given. Pass no more than one." << endl;
        validArgsNumb = false;
    }else{

        //Iteration through the "value" array is done and every
        //element is checked to make sure it's not a letter using
        //the "isdigit()" function.
        const int LENGTH = strlen(value);
        int iteration = 0;
        while (iteration != LENGTH) {
            if (!isdigit(*(value + iteration))){
                charsAreDigits = false;
            };
            iteration++;
        };

        //Should the "charsAreDigits" flag still not be changed,
        //then the value is converted into an "int" using the
        //"atoi()" function.
        if (charsAreDigits == true){
            int intValue;
            intValue = atoi(value);
            if ((intValue < 1) || (intValue > 20)){charsAreDigits = false;};
        };
    };

    //Both bools are checked in order to return the appropriate result.
    if((validArgsNumb == true) && (charsAreDigits == true)){return true;}
    else{

        if(charsAreDigits == false){
            cout << "ERROR: Only number values between \"1\" and \"20\" are acceptable." << endl;
        };

        return false;
    };
}

//- - - - - - - - - - - - - - - - - - - - -
//     ***RANDOM NUMBER GENERATOR***
//- - - - - - - - - - - - - - - - - - - - -
void randomIntGenerator(int *randomIntArray, const int ARRAY_SIZE){
    int pseudoRandInt;
    srand(time(NULL));
    for(int index = 0; index < ARRAY_SIZE; index++){
        pseudoRandInt = rand() % 9000 + 1000;
        *(randomIntArray + index) = pseudoRandInt;
    };
}