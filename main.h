/* ************************************************************************
 * Name: Flavio C. Palacios Tinoco
 * Assignment: 03
 * Purpose: Main program header
 * Notes:
 * *************************************************************************/
#ifndef MAIN_H
#define MAIN_H

#include "linkedList.h"
#include <iostream>
#include <cstring>  //For strlen().
#include <cstdlib>  //For rand(), isdigit(), and atoi().
#include <ctime>    //For using the system clock in the random int generator.
using namespace std;

//The input validation function that checks all given arguments to make sure
//only one argument was fed, that the given argument is not a letter, and to
//make sure the given argument is in the acceptable range.
bool checkArgs(int, char *);

//This function generates random numbers and stores them in an array of the size
//according to the argument number given through the command line.
void randomIntGenerator(int *, const int);

#endif //MAIN_H
