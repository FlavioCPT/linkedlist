
/* ************************************************************************
 * Name: Flavio C. Palacios Tinoco
 * Assignment: 03
 * Purpose: Linked List function implementations
 * Notes:
 * *************************************************************************/
#include "linkedList.h"

//- - - - - - - - - - - - - - - - - -
//   ***ADD A NODE TO THE LIST***
//- - - - - - - - - - - - - - - - - -
void List::addNode(int addedData){
    //Any time this function is called, the "count" accumulator keeps track.
    List::count++;

    //When there's no nodes, head and tail pointers aim at the new node
    //and its "forward" and "backward" pointers point to NULL.
    if (headptr == nullptr){
        createdNodePtr = new Node;
        createdNodePtr->id = addedData;
        createdNodePtr->forward = nullptr;
        createdNodePtr->backward = nullptr;
        headptr = createdNodePtr;
        tailptr = createdNodePtr;

        //When there's at least one node...
    }else{
        createdNodePtr = new Node;
        createdNodePtr->id = addedData;

        tempptrOne = headptr;

        //This happens only when there's one node and the new node is to
        //become the new head.
        if((tempptrOne->forward == nullptr) && (createdNodePtr->id <= tempptrOne->id)){
            tempptrOne->backward = createdNodePtr;
            createdNodePtr->forward = tempptrOne;
            createdNodePtr->backward = nullptr;
            headptr = createdNodePtr;
        }

        //Again when there's only one node, but now the new node will become the tail.
        else if((tempptrOne->forward == nullptr) && (createdNodePtr->id >= tempptrOne->id)){
            tempptrOne->forward = createdNodePtr;
            createdNodePtr->forward = nullptr;
            createdNodePtr->backward = tempptrOne;
            tailptr = createdNodePtr;
        }

        else if(tempptrOne->forward != nullptr){
            //The node "tempptrH" will point to, after exiting the loop, will either have
            //an "id" value greater than "createdNodePtr" or it will be the last node due
            //to the case that the new node should belong at the end of the list.
            while(((createdNodePtr->id) >= (tempptrOne->id)) && (tempptrOne->forward != nullptr)){
                tempptrOne = tempptrOne->forward;
            };

            //This is where the new node is added as the new tail.
            if((tempptrOne->forward == nullptr) && (createdNodePtr->id >= tempptrOne->id)){
                    createdNodePtr->forward = nullptr;
                    createdNodePtr->backward = tempptrOne;
                    tempptrOne->forward = createdNodePtr;
                    tailptr = createdNodePtr;
            }

            //Here the new node is added as the new head.
            else if (tempptrOne->backward == nullptr){
                createdNodePtr->backward = nullptr;
                tempptrOne->backward = createdNodePtr;
                createdNodePtr->forward = tempptrOne;
                headptr = createdNodePtr;
            }
            //It might happen that the new node belongs in between two other nodes.
            else{
                tempptrTwo = tempptrOne;
                createdNodePtr->forward = tempptrOne;
                tempptrOne = tempptrOne->backward;
                tempptrOne->forward = createdNodePtr;
                createdNodePtr->backward = tempptrOne;
                tempptrOne = tempptrTwo;
                tempptrOne->backward = createdNodePtr;
            };
        };
    };
}

//- - - - - - - - - - - - - - - - - - -
//   ***DELETE NODE FROM THE LIST***
//- - - - - - - - - - - - - - - - - - -
void List::deleteNode(int nodeToDel){
    //Pointer declared to delete a node.
    struct Node *delNodePtr = nullptr;

    if(nodeToDel == 0){
        cout << "There are no nodes to delete." << endl;
        delete delNodePtr;
    }

    //If only one head node exists, it is deleted.
    else if((nodeToDel == 1) && (headptr->forward == nullptr)){
        delNodePtr = headptr;
        headptr = nullptr;
        tailptr = nullptr;
        delete delNodePtr;
        List::count--;
    }

    //If the head node is deleted, "headptr" moves to the next node.
    else if((nodeToDel == 1) && (headptr->forward != nullptr)){
        delNodePtr = headptr;
        headptr = headptr->forward;
        headptr->backward = nullptr;
        delete delNodePtr;
        List::count--;
    }

    //If only one tail node exists, it is deleted.
    else if((nodeToDel == List::count) && (tailptr->backward == nullptr)){
        delNodePtr = tailptr;
        headptr = nullptr;
        tailptr = nullptr;
        delete delNodePtr;
        List::count--;
    }

    //Deleting the tail node makes the "tailptr" move to the previous node.
    else if((nodeToDel == List::count) && (tailptr->backward != nullptr)){
        delNodePtr = tailptr;
        tailptr = tailptr->backward;
        tailptr->forward = nullptr;
        delete delNodePtr;
        List::count--;
    }

    //This condition here would mean a value in the middle of the list is being
    //deleted.
    else{
        tempptrOne = headptr;
        tempptrTwo = headptr;

        //Here the "tempptrOne" advances to the Nth node (being the 2nd,3rd,4th,...).
        //After the loop terminates, "tempptrTwo" will point to the previous node that
        //"tempptrOne" pointed to.
        int accumulator = 1;
        while(accumulator != nodeToDel){
            tempptrTwo = tempptrOne;
            tempptrOne = tempptrOne->forward;
            accumulator++;
        };

        delNodePtr = tempptrOne;
        tempptrOne = tempptrOne->forward;
        tempptrOne->backward = tempptrTwo;
        tempptrTwo->forward = tempptrOne;
        delete delNodePtr;
        List::count--;
    };

}

//- - - - - - - - - - - - - - - - -
//   ***PRINT ALL LIST NODES***
//- - - - - - - - - - - - - - - - -
void List::printList(){
    tempptrOne = headptr;
    cout << "Here are the Nodes: " << endl;
    cout << "- - - - - - - - - -" << endl;
    while(tempptrOne != nullptr){
        cout << tempptrOne->id << endl;
        tempptrOne = tempptrOne->forward;
    };
}

//- - - - - - - - - - - - - - - -
//   ***ACCESSOR FOR COUNT***
//- - - - - - - - - - - - - - - -
int List::getCount(){
    return count;
}

//- - - - - - - - - - - -
//   ***CONSTRUCTOR***
//- - - - - - - - - - - -
List::List(){
    createdNodePtr = nullptr;
    headptr = nullptr;
    tailptr = nullptr;
    tempptrOne = nullptr;
    tempptrTwo = nullptr;
}